package creations.nag.planner.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.IOException;

/**
 * Created by natha on 10/14/2017.
 */

public class DBOpenHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "planner.sqlite";
    public static final int DB_VERSION = 1;

    public DBOpenHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_NOTES_TABLE);
        db.execSQL(CREATE_TODOS_TABLE);
        db.execSQL(CREATE_EVENTS_TABLE);
        db.execSQL(CREATE_DATES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DELETE_DATES_TABLE);
        db.execSQL(CREATE_DATES_TABLE);

        db.execSQL(DELETE_NOTES_TABLE);
        db.execSQL(CREATE_NOTES_TABLE);

        db.execSQL(DELETE_TODOS_TABLE);
        db.execSQL(CREATE_TODOS_TABLE);

        db.execSQL(CREATE_EVENTS_TABLE);
        db.execSQL(DELETE_EVENTS_TABLE);
    }

    /**
     * This table will contain all dates that have notes, events, and/or todos.
     */
    private static final String CREATE_DATES_TABLE =
            "CREATE TABLE Dates" +
                    "(" +
                    "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "Dates TEXT," +
                    "Day INTEGER," +
                    "Month INTEGER," +
                    "Year INTEGER" +
                    ")";

    private static final String DELETE_DATES_TABLE =
            "DROP TABLE IF EXISTS " + "Dates";

    private static final String CREATE_NOTES_TABLE =
            "CREATE TABLE Notes" +
                    "(" +
                    "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "Date TEXT," +
                    "Notes TEXT" +
                    ");";

    private static final String DELETE_NOTES_TABLE =
            "DROP TABLE IF EXISTS " + "Notes";

    private static final String CREATE_TODOS_TABLE =
            "CREATE TABLE Todos" +
                    "(" +
                    "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "Date TEXT," +
                    "Todos TEXT," +
                    "Hour TEXT," +
                    "Checkmark TEXT," +
                    "Alarm TEXT" +
                    ");";

    private static final String DELETE_TODOS_TABLE =
            "DROP TABLE IF EXISTS " + "Todos";

    private static final String CREATE_EVENTS_TABLE =
            "CREATE TABLE Events" +
                    "(" +
                    "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "Date TEXT," +
                    "Events TEXT," +
                    "Hour TEXT," +
                    "Alarm TEXT" +
                    ")";

    private static final String DELETE_EVENTS_TABLE =
            "DROP TABLE IF EXISTS " + "Events";
}
