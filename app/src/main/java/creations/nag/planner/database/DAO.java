package creations.nag.planner.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;

import creations.nag.planner.models.Dates;
import creations.nag.planner.models.Events;
import creations.nag.planner.models.Model;
import creations.nag.planner.models.Todos;

public class DAO {

    //Singleton method
    public static DAO SINGLETON = new DAO();

    private SQLiteDatabase db;

    private DAO() {
        db = Model.SINGLETON.getDb().getSqlDb();
    }

    //Methods

        /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ DATES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    public void getDates(HashMap<String, HashSet<Dates>> allDates)  {
        Cursor cursor = db.rawQuery("select * from " + "Dates", null);
        allDates.clear();

        try {
            cursor.moveToFirst();
            if (cursor.getString(cursor.getColumnIndex("Dates")) != null) {
                while (!cursor.isAfterLast()) {
                    String date = cursor.getString(cursor.getColumnIndex("Dates"));
                    int day = cursor.getInt(cursor.getColumnIndex("Day"));
                    int month = cursor.getInt(cursor.getColumnIndex("Month"));
                    int year = cursor.getInt(cursor.getColumnIndex("Year"));

                    Dates storedDate = new Dates(day, month, year);
                    Log.d("@getDates", date);

                    if (!allDates.containsKey(date)) {
                        HashSet<Dates> setDates = new LinkedHashSet<>();
                        setDates.add(storedDate);
                        allDates.put(date, setDates);
                    }
                    else {
                        HashSet<Dates> setTodos = allDates.get(date);
                        setTodos.add(storedDate);
                    }
                    cursor.moveToNext();
                }
            }

        } catch (Exception e) {
            Log.d("@getDates-No Dates", e.getMessage());
        } finally {
            cursor.close();
        }
    }

    public void putDates(String date) {

        ContentValues values = new ContentValues();
        values.put("Dates", date);
        values.put("Day", Model.SINGLETON.getDay());
        values.put("Month", Model.SINGLETON.getMonth());
        values.put("Year", Model.SINGLETON.getYear());
        long id_debug = -1;

        if (!Model.SINGLETON.doesDateExist(date)) { // Add the date to the database if it doesn't exist yet.
            id_debug = db.insert("Dates", null, values);
        }

        Log.d("Did date save/update?", Long.toString(id_debug));
    }

    public void deleteDates(String date) {
        long id_debug = -1;

        String[] whereArgument = {date};
        try {
            id_debug = db.delete("Dates", "Dates = ?", whereArgument);
        } catch (Exception e) {
            Log.d("@deleteDate", e.getMessage());
        }
        Log.d("Did Date get deleted?", Long.toString(id_debug));
    }

        /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ NOTES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    public String getNotes() {
        String result = "";

        String[] selectionArguments = { Model.SINGLETON.getSavedDate() };
        Cursor cursor = db.rawQuery("select Notes from " + "Notes" +
                " where " + "Date = ?", selectionArguments);
        try {
            cursor.moveToFirst();
            if (cursor.getString(cursor.getColumnIndex("Notes")) != null) {
                String notes = cursor.getString(cursor.getColumnIndex("Notes"));
                Log.d("@getNotes", notes);
                result = notes;
            }

        } catch (Exception e) {
            Log.d("@getNotes-No Notes", e.getMessage());
        } finally {
            cursor.close();
        }

        return result;
    }

    public void putNotes(String notes, Boolean notesExist) {
        ContentValues values = new ContentValues();
        values.put("Date", Model.SINGLETON.getSavedDate());
        values.put("Notes", notes);
        long id_debug;

        if (!notesExist) { // If notes don't exist already then save new ones
            id_debug = db.insert("Notes", null, values);
        }
        else { // If notes do exist, then update them
            id_debug = db.update("Notes", values, "Date = " + Model.SINGLETON.getSavedDate(), null);
        }

        Log.d("Did note save/update?", Long.toString(id_debug));
    }

    public void deleteNotes() {
        long id_debug = -1;

        String[] whereArgument = { Model.SINGLETON.getSavedDate() };
        try {
            id_debug = db.delete("Notes", "Date = ?", whereArgument);
        } catch (Exception e) {
            Log.d("@deleteNotes", e.getMessage());
        }
        Log.d("Did notes get deleted?", Long.toString(id_debug));

    }

        /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ TODOS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    public void getTodos(String savedDate, HashMap<String, LinkedHashSet<Todos>> allTodos) {
        String[] selectionArguments = { savedDate };
        Cursor cursor = null;
        allTodos.clear(); // Clear map everytime we're getting todos for a day.

        try {
            cursor = db.rawQuery("select * from " + "Todos" + " where " + "Date = ?", selectionArguments);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                long id_debug = cursor.getInt(cursor.getColumnIndex("id"));
                String date = cursor.getString(cursor.getColumnIndex("Date")); //Used for adding the todos at the proper date
                String todo = cursor.getString(cursor.getColumnIndex("Todos"));
                String checkmark = cursor.getString(cursor.getColumnIndex("Checkmark"));
                String hour = cursor.getString(cursor.getColumnIndex("Hour"));
                String hasAlarm = cursor.getString(cursor.getColumnIndex("Alarm"));

                Todos storedTodo = new Todos(todo, hour, checkmark, hasAlarm);
                Log.d("@getTodos", storedTodo.toString());

                if (allTodos.size() == 0) {
                    LinkedHashSet<Todos> newTodos = new LinkedHashSet<>();
                    newTodos.add(storedTodo);
                    allTodos.put(savedDate, newTodos);
                }
                else if (!Model.SINGLETON.doesTodoExist(todo)) { // If to-do doesn't exist, then put it in the map
                    LinkedHashSet<Todos> setTodos = allTodos.get(date);
                    setTodos.add(storedTodo);
                }

                cursor.moveToNext();
            }

        } catch (Exception e) {
            Log.d("@getEvents-No Events", e.getMessage());
        } finally {
            assert cursor != null; // Defensive programming just in case
            cursor.close();
        }
    }

    public void putTodos(String date, String newTodo, String hour, String checkmark, String hasAlarm, String oldTodo) {
        ContentValues values = new ContentValues();
        values.put("Date", date);
        values.put("Todos", newTodo);
        values.put("Hour", hour);
        values.put("Checkmark", checkmark);
        values.put("Alarm", hasAlarm);
        long id_debug = -1;

        if (!Model.SINGLETON.doesTodoExist(newTodo)) { // Doesn't allow duplicate todos in database
            if (!Model.SINGLETON.replaceTodoByTodo(oldTodo, newTodo)) { // If to-do title doesn't get replaced/isn't new,
                id_debug = db.insert("Todos", null, values); // put it in the database
            }
            else {
                String[] whereArguments = {oldTodo};
                id_debug = db.update("Todos", values, "Todos = ?", whereArguments);
            }
        }

        Log.d("Did Todos save/update?", Long.toString(id_debug));
    }

    public void updateCheckedTodo(String todo, String checkmark) {
        ContentValues values = new ContentValues();
        values.put("Checkmark", checkmark);
        long id_debug;

        String[] whereArguments = {todo};
        id_debug = db.update("Todos", values, "Todos = ?", whereArguments);

        Log.d("Did Todos save/update?", Long.toString(id_debug));
    }

    public void deleteTodo(String todo) {
        long id_debug = -1;

        String[] whereArgument = {todo};
        try {
            id_debug = db.delete("Todos", "Todos = ?", whereArgument);
        } catch (Exception e) {
            Log.d("@deleteTodo", e.getMessage());
        }
        Log.d("Did Todo get deleted?", Long.toString(id_debug));

    }

        /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ EVENTS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    public void getEvents(String savedDate, HashMap<String, LinkedHashSet<Events>> allEvents) {
        String[] selectionArguments = { savedDate };
        Cursor cursor = null;
        allEvents.clear();

        try {
            cursor = db.rawQuery("select * from " + "Events" + " where " + "Date = ?", selectionArguments);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                String date = cursor.getString(cursor.getColumnIndex("Date")); //Used for adding the todos at the proper date
                String event = cursor.getString(cursor.getColumnIndex("Events"));
                String hour = cursor.getString(cursor.getColumnIndex("Hour"));
                String hasAlarm = cursor.getString(cursor.getColumnIndex("Alarm"));

                Events storedEvent = new Events(event, hour, hasAlarm);
                Log.d("@getEvents", storedEvent.toString());

                if (allEvents.size() == 0) {
                    LinkedHashSet<Events> newTodos = new LinkedHashSet<>();
                    newTodos.add(storedEvent);
                    allEvents.put(savedDate, newTodos);
                }
                else if (!Model.SINGLETON.doesHourHaveEvent(hour)) {
                    LinkedHashSet<Events> setTodos = allEvents.get(date);
                    setTodos.add(storedEvent);
                }

                cursor.moveToNext();
            }

        } catch (Exception e) {
            Log.d("@getEvents-No Events", e.getMessage());
        } finally {
            assert cursor != null; // Defensive programming just in case
            cursor.close();
        }
    }

    public void putEvents(String date, String newEvent, String hour, String hasAlarm, String oldEvent) {

        ContentValues values = new ContentValues();
        values.put("Date", date);
        values.put("Events", newEvent);
        values.put("Hour", hour);
        values.put("Alarm", hasAlarm);
        long id_debug = -1;

        if (!Model.SINGLETON.doesHourHaveEvent(hour)) { // Doesn't allow duplicate events at same time in database
            id_debug = db.insert("Events", null, values); // put it in the database
        }
        else { // Update event at a given time
            String[] whereArguments = {oldEvent, hour};
            id_debug = db.update("Events", values, "Events = ? AND Hour = ?", whereArguments);
        }

        Log.d("Did Events save/update?", Long.toString(id_debug));
    }

    public void deleteEvent(String event) {
        long id_debug = -1;

        String[] whereArgument = {event};
        try {
            id_debug = db.delete("Events", "Events = ?", whereArgument);
        } catch (Exception e) {
            Log.d("@deleteEvent", e.getMessage());
        }
        Log.d("Did event get deleted?", Long.toString(id_debug));

    }
}