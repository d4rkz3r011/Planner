package creations.nag.planner.database;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by raulbr on 2/12/17.
 */


/**
 * This will use Sqlite database
 */
public class Database {

    private DBOpenHelper dbOpenHelper;
    private Context baseContext;
    private SQLiteDatabase sqlDb;

    public Database(Context baseContext) {
        this.baseContext = baseContext;
        dbOpenHelper = new DBOpenHelper(baseContext);
        sqlDb = dbOpenHelper.getWritableDatabase();
    }

    public void close() {
        sqlDb.close();
        dbOpenHelper.close();
    }

    public void dropAllTables() {
//        sqlDb.execSQL("delete from "+ "Notes");
        sqlDb.delete("Notes", null, null);
    }

    public SQLiteDatabase getSqlDb() {
        return sqlDb;
    }
}