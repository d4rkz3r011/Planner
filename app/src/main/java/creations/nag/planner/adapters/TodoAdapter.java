package creations.nag.planner.adapters;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;

import creations.nag.planner.R;
import creations.nag.planner.database.DAO;
import creations.nag.planner.day.AddEventActivity;
import creations.nag.planner.models.Model;
import creations.nag.planner.models.Todos;

/**
 * Specific Adaptor class needed to set the RecyclerView data member.
 * Puts in a list_filter_item object into the fragment_filter_list xml.
 * Also calls the FilterHolder class to bind text to the filter items
 *
 * Created by NAG on 10/25/17.
 */

public class TodoAdapter extends RecyclerView.Adapter<TodoHolder> {

    private Map<String, LinkedHashSet<Todos>> todos = Model.SINGLETON.getTodos();
    private String dateKey = Model.SINGLETON.getSavedDate();
    private FragmentActivity todoFragActivity;

    public TodoAdapter(FragmentActivity todoFragActivity) {
        this.todoFragActivity = todoFragActivity;
    }

    @Override
    public TodoHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        final LayoutInflater layoutInflater = LayoutInflater.from(todoFragActivity); //Grabs layout from activity
        View view = layoutInflater.inflate(R.layout.day_todo, parent, false);
        return new TodoHolder(view, todoFragActivity);
    }

    @Override
    public void onBindViewHolder(TodoHolder holder, int position)
    {
        if (todos.containsKey(dateKey)) { // Does the date have a to-do?
            for (Map.Entry<String, LinkedHashSet<Todos>> map : todos.entrySet()) { // Go through the map of todos
                if (map.getKey().equals(dateKey)) { // Get the key that matches the date selected
                    Iterator<Todos> iter = todos.get(dateKey).iterator(); // Create a Todos iterator
                    for (int i = 0; i < map.getValue().size(); i++) { // Go through the map of Todos
                        Todos temp = iter.next(); // Grab the next Todos object in the set of the map
                        if (i == position && temp != null) { // This properly aligns the todos in the recycler view
                            holder.bindTextToTodos(temp.getTodo());
                        }
                    }
                }
            }
        }
    }

    /**
     * Gets the size of the filter List in the ModelHolder class
     *
     * @return Size of list
     */
    @Override
    public int getItemCount() { // The amount of todos in ßthe day
        if (!todos.isEmpty()) {
            for (Map.Entry<String, LinkedHashSet<Todos>> map : todos.entrySet()) {
                if (map.getKey().equals(dateKey)) {
                    return map.getValue().size();
                }
            }
        }
        return 0;
    }

    public void updateTodosInView() {
        Model.SINGLETON.updateTodos();
        notifyDataSetChanged();
    }

}
