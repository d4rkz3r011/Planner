package creations.nag.planner.adapters;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import creations.nag.planner.R;
import creations.nag.planner.database.DAO;
import creations.nag.planner.day.AddEventActivity;
import creations.nag.planner.models.Model;

/**
 * This class stores all the filter titles, subtitles, and event switches.
 * It also sets each hour.
 * This is created for ease of accessing and updating all the filter titles, subtitles and switches
 *
 * Created by NAG on 10/25/17.
 */

public class TodoHolder extends RecyclerView.ViewHolder   {
    private TextView todo;
    private CheckBox checkBox;
    private String isChecked;
    private FragmentActivity todoFragActivity;


    public TodoHolder(View view, FragmentActivity fragmentActivity) {
        super(view);
        todoFragActivity = fragmentActivity;

        ConstraintLayout constraintLayout = (ConstraintLayout) view.findViewById(R.id.todoItemHolder);
        constraintLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent intent = new Intent(todoFragActivity, AddEventActivity.class);
                intent.putExtra("Edit Todo", todo.getText().toString());
                intent.putExtra("hour", Model.SINGLETON.getHourForTodo(todo.getText().toString()));
                intent.putExtra("checkmark", Model.SINGLETON.getCheckmarkForTodo(isChecked));
                intent.putExtra("alarm", Model.SINGLETON.getAlarmForTodo(todo.getText().toString()));
                todoFragActivity.startActivity(intent);
                return false;
            }
        });

        todo = (TextView) view.findViewById(R.id.textview_dayTodo);
        todo.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent intent = new Intent(todoFragActivity, AddEventActivity.class);
                intent.putExtra("Edit Todo", todo.getText().toString());
                intent.putExtra("hour", Model.SINGLETON.getHourForTodo(todo.getText().toString()));
                intent.putExtra("checkmark", Model.SINGLETON.getCheckmarkForTodo(isChecked));
                intent.putExtra("alarm", Model.SINGLETON.getAlarmForTodo(todo.getText().toString()));
                todoFragActivity.startActivity(intent);
                return false;
            }
        });


        checkBox = (CheckBox) view.findViewById(R.id.checkBox);
        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isChecked == null) { isChecked = "false"; }
                else if (isChecked.equals("false")) { isChecked = "true"; } // If isChecked is false, make true
                else { isChecked = "false"; } // If isChecked is true, make false;
                DAO.SINGLETON.updateCheckedTodo(todo.getText().toString(), isChecked); // Add 1 to get proper database id
            }
        });
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    TodoHolder.this.isChecked = "true";
                }
                else {
                    TodoHolder.this.isChecked = "false";
                }
            }
        });


    }

    private Boolean stringToBool(String bool) {
        if (bool.equals("true")) { return true; }
        else { return false; }
    }

    public void bindTextToTodos(final String todo_title) {
        todo.setText(todo_title);
        isChecked = Model.SINGLETON.getCheckmarkForTodo(todo.getText().toString());
        checkBox.setChecked(stringToBool(isChecked));
    }

}
