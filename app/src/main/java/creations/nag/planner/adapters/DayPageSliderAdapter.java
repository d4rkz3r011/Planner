package creations.nag.planner.adapters;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import creations.nag.planner.day.DayFragment;
import creations.nag.planner.day.DayNoteFragment;
import creations.nag.planner.day.DayTodoFragment;

/**
 * This adapter allows for sliding between the day, day todos, and day notes views.
 * The day notes view will be the first one that appears.
 *
 * Created by NAG on 10/10/17.
 */

public class DayPageSliderAdapter extends FragmentStatePagerAdapter {

    //Constructor
    public DayPageSliderAdapter(FragmentManager fm) {
        super(fm);
    }

    //Methods
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: return new DayTodoFragment();
            case 1: return new DayFragment();
            case 2: return new DayNoteFragment();
            default: return new DayFragment();
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public void startUpdate(ViewGroup container) {
        super.startUpdate(container);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        return super.instantiateItem(container, position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        super.setPrimaryItem(container, position, object);
    }

    @Override
    public void finishUpdate(ViewGroup container) {
        super.finishUpdate(container);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return super.isViewFromObject(view, object);
    }

    @Override
    public Parcelable saveState() {
        return super.saveState();
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
        super.restoreState(state, loader);
    }
}
