package creations.nag.planner.adapters;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.LinkedHashSet;
import java.util.Map;

import creations.nag.planner.R;
import creations.nag.planner.day.AddEventActivity;
import creations.nag.planner.models.Events;
import creations.nag.planner.models.Model;

/**
 * This class stores all the filter titles, subtitles, and event switches.
 * It also sets each hour.
 * This is created for ease of accessing and updating all the filter titles, subtitles and switches
 *
 * Created by NAG on 10/25/17.
 */

public class EventHolder extends RecyclerView.ViewHolder {

    private TextView hour;
    private TextView event;
    private String dateKey;
    private FragmentActivity eventFragActivity;
    private Map<String, LinkedHashSet<Events>> events;

    public EventHolder(View view, FragmentActivity fragmentActivity)
    {
        super(view);
        eventFragActivity = fragmentActivity;
        events = Model.SINGLETON.getEvents();
        dateKey = Model.SINGLETON.getSavedDate();
        hour = (TextView) view.findViewById(R.id.textview_hour);

        ConstraintLayout constraintLayout = (ConstraintLayout) view.findViewById(R.id.eventItemHolder);
        constraintLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (!event.getText().toString().equals("")) {
                    Intent intent = new Intent(eventFragActivity, AddEventActivity.class);
                    intent.putExtra("Edit Event", event.getText().toString());
                    intent.putExtra("hour", Model.SINGLETON.getHourForEvent(event.getText().toString()));
                    intent.putExtra("alarm", Model.SINGLETON.getAlarmForEvent(event.getText().toString()));
                    eventFragActivity.startActivity(intent);
                }
                return false;
            }
        });

        event = (TextView) view.findViewById(R.id.textview_event);
        event.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (!event.getText().toString().equals("")) {
                    Intent intent = new Intent(eventFragActivity, AddEventActivity.class);
                    intent.putExtra("Edit Event", event.getText().toString());
                    intent.putExtra("hour", Model.SINGLETON.getHourForEvent(event.getText().toString()));
                    intent.putExtra("alarm", Model.SINGLETON.getAlarmForEvent(event.getText().toString()));
                    eventFragActivity.startActivity(intent);
                }
                return false;
            }
        });
    }

    public void bindTextToEvents(final String hour_time)
    {
        hour.setText(hour_time);
        String allEvents = "";
        if (events.containsKey(dateKey)) { // Are there events in the day?
            for (Map.Entry<String, LinkedHashSet<Events>> map : events.entrySet()) {
                // Goes through set of Events at every map key
                for (Events e : map.getValue()) {
                    // If the time of event matches the hour we're looking at, add text
                    if (e.getHour().equals(hour_time)) {
                        if (!allEvents.equals("")) {
                            allEvents += "\n";
                        } // Put new lines when adding events to an hour.
                        allEvents += e.getEvent();
                    }
                }
            }
        }

        event.setText(allEvents);
    }
}
