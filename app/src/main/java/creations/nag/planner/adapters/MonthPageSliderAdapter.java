package creations.nag.planner.adapters;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import creations.nag.planner.month.MonthFragment;
import creations.nag.planner.month.MonthNoteFragment;
import creations.nag.planner.month.MonthTodoFragment;

/**
 * Created by NAG on 10/10/17.
 */

public class MonthPageSliderAdapter extends FragmentStatePagerAdapter {

    //Constructor
    public MonthPageSliderAdapter(FragmentManager fm) {
        super(fm);
    }

    //Methods
    @Override
    public Fragment getItem(int position) {
        switch (position) {
//            case 0: return new MonthTodoFragment();
//            case 1: return new MonthFragment();
//            case 2: return new MonthNoteFragment();
            default: return new MonthFragment();
        }
    }

    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public void startUpdate(ViewGroup container) {
        super.startUpdate(container);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        return super.instantiateItem(container, position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        super.setPrimaryItem(container, position, object);
    }

    @Override
    public void finishUpdate(ViewGroup container) {
        super.finishUpdate(container);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return super.isViewFromObject(view, object);
    }

    @Override
    public Parcelable saveState() {
        return super.saveState();
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
        super.restoreState(state, loader);
    }
}
