package creations.nag.planner.adapters;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

/**
 * Created by NAG on 10/23/17.
 */

public class TimeSpinnerAdapter implements AdapterView.OnItemSelectedListener {

    private Spinner timeSpinner;

    public TimeSpinnerAdapter(Spinner timeSpinner) {
        this.timeSpinner = timeSpinner;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        timeSpinner.setSelection(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {}
}
