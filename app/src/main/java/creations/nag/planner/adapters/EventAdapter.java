package creations.nag.planner.adapters;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;

import creations.nag.planner.R;
import creations.nag.planner.models.Events;
import creations.nag.planner.models.Model;

/**
 * Specific Adaptor class needed to set the RecyclerView data member.
 * Puts in a list_filter_item object into the fragment_filter_list xml.
 * Also calls the FilterHolder class to bind text to the filter items
 *
 * Created by NAG on 10/25/17.
 */

public class EventAdapter extends RecyclerView.Adapter<EventHolder> {

    private FragmentActivity eventFragActivity;

    public EventAdapter(FragmentActivity eventFragActivity) {
        this.eventFragActivity = eventFragActivity;
    }

    @Override
    public EventHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        LayoutInflater layoutInflater = LayoutInflater.from(eventFragActivity); //Grabs layout from activity
        View v = layoutInflater.inflate(R.layout.day_eventhour, parent, false);

        return new EventHolder(v, eventFragActivity);
    }

    @Override
    public void onBindViewHolder(EventHolder holder, int position)
    {
        String hour = Model.SINGLETON.getListOfHours24().get(position);
        holder.bindTextToEvents(hour);
    }

    /**
     * Gets the size of the filter List in the ModelHolder class
     *
     * @return Size of list
     */
    @Override
    public int getItemCount() { return 24; } // The amount of hours in the day

    public void updateEventsInView() {
        Model.SINGLETON.updateEvents();
        notifyDataSetChanged();
    }
}
