package creations.nag.planner.month;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;

import creations.nag.planner.R;
import creations.nag.planner.calendar.data.Month;
import creations.nag.planner.calendar.view.CalendarView;
import creations.nag.planner.day.DayActivity;
import creations.nag.planner.models.Dates;
import creations.nag.planner.models.Model;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MonthFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MonthFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MonthFragment extends Fragment {

    private CalendarView calendarView;

    public MonthFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MonthFragment.
     */
    public static MonthFragment newInstance() {
        MonthFragment fragment = new MonthFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_month, container, false);

        if (getActivity().getIntent().getExtras() != null) {
            Bundle bundle = getActivity().getIntent().getExtras();
            if (bundle.getString("startup_date") != null) { // Only runs on app startup
                int year = Integer.parseInt(new SimpleDateFormat("yyyy", Locale.US).format(new Date()));
                int month = Integer.parseInt(new SimpleDateFormat("MM", Locale.US).format(new Date())); // Subtract 1 since January starts at index 0.
                int date = Integer.parseInt(new SimpleDateFormat("d", Locale.US).format(new Date()));
                getActivity().getIntent().removeExtra("startup_date"); // Remove extra to avoid extra Day Activities
                startDayActivity(date, month, year);
            }
        }

        calendarView = (CalendarView) view.findViewById(R.id.calendarView);
        calendarView.setOnDayClickListener(new CalendarView.OnDayClickListener() {
            @Override
            public void onClick(int date, int month, int year, boolean hasEvent) {
                startDayActivity(date, month, year);
            }
        });
//        Model.SINGLETON.setCalendarAdapter(this);
//        addEventsToCalendar();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        addEventsToCalendar();
    }

    private void addEventsToCalendar() {
        // If there are any events or todos, search the map of dates.

        Model.SINGLETON.getCalendarAdapter().resetCalendarEvents();
        for (Map.Entry<String, HashSet<Dates>> map : Model.SINGLETON.getDates().entrySet()) {
            for (Dates date : map.getValue()) {
                calendarView.addEvent(date.getDay(), date.getMonth(), date.getYear());
            }
        }
    }

    private void startDayActivity(int date, int month, int year) {
        Intent intent = new Intent(getContext(), DayActivity.class);
        intent.putExtra("month", month);
        intent.putExtra("date", date);
        intent.putExtra("year", year);
        startActivity(intent);
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    /* Created by jpttrindade on 10/09/16. https://github.com/jpttrindade/CalendarView

    private CalendarView calendarView;
   // private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //editText = (EditText) findViewById(R.id.editText);
        calendarView = (CalendarView) findViewById(R.id.calendarView);
        calendarView.updateTodosInView(16,9,2016);
        calendarView.updateTodosInView(17,9,2016);
        calendarView.updateTodosInView(18,9,2016);
        calendarView.updateTodosInView(19,9,2016);

        calendarView.setOnDayClickListener(new CalendarView.OnDayClickListener() {
            @Override
            public void onClick(int day, int month, int year, boolean hasEvent) {
                Toast.makeText(MainActivity.this, day+"/"+month+"/"+year + " hasEvent="+hasEvent, Toast.LENGTH_SHORT).show();
                if (hasEvent) {
                    calendarView.deleteEvent(day, month, year);
                }
            }
        });

    }


    public void updateTodosInView(View view) {
        String strDate = "";// editText.getText().toString();
        if (strDate.isEmpty()) {
            Toast.makeText(this, "define a day", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "date="+strDate, Toast.LENGTH_SHORT).show();

            String[] date = strDate.split("/");

            int day = Integer.parseInt(date[0]);
            int month = Integer.parseInt(date[1]);
            int year = Integer.parseInt(date[2]);
            calendarView.updateTodosInView(day, month, year);

        }
    }


    public void showDatePickerDialog(View v) {
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.setCalendarView(calendarView);
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }


    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        private CalendarView calendarView;

        public void setCalendarView(CalendarView calendarView) {
            this.calendarView = calendarView;
        }
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            calendarView.updateTodosInView(day, month+1, year);
        }
    }
     */


    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        private CalendarView calendarView;

        public void setCalendarView(CalendarView calendarView) {
            this.calendarView = calendarView;
        }
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            calendarView.addEvent(day, month, year);
        }
    }

    // GETTERS
    public CalendarView getCalendarView() {
        return calendarView;
    }
}
