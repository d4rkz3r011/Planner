package creations.nag.planner.month;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;

import creations.nag.planner.models.Model;
import creations.nag.planner.R;
import creations.nag.planner.SettingsActivity;
import creations.nag.planner.adapters.MonthPageSliderAdapter;

public class MonthActivity extends AppCompatActivity {

    private MonthFragment monthFragment;
    private MonthTodoFragment monthTodoFragment;
    private MonthNoteFragment monthNoteFragment;

    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private ViewPager viewPager;

    /**
     * The page adapter, which provides the pages to the view pager widget.
     */
    private MonthPageSliderAdapter monthPageSliderAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        setContentView(R.layout.activity_month);
        getActionBar();

        viewPager = (ViewPager) findViewById(R.id.view_pager_month);
        monthPageSliderAdapter = new MonthPageSliderAdapter(this.getSupportFragmentManager());
        viewPager.setAdapter(monthPageSliderAdapter);
        viewPager.setCurrentItem(1);

        // Create the fragments
        if (monthFragment == null) { monthFragment = MonthFragment.newInstance(); }
        if (monthTodoFragment == null) { monthTodoFragment = MonthTodoFragment.newInstance(); }
        if (monthNoteFragment == null) { monthNoteFragment = MonthNoteFragment.newInstance(); }

        Model.SINGLETON.setMonthActivity(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.daymonth_options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.settingsButton:
                startActivity(new Intent(getBaseContext(), SettingsActivity.class));
                break;
            default:
                break;
        }
        return true;
    }



    public MonthFragment getMonthFragment() {
        return monthFragment;
    }

    public MonthTodoFragment getMonthTodoFragment() {
        return monthTodoFragment;
    }

    public MonthNoteFragment getMonthNoteFragment() {
        return monthNoteFragment;
    }

}
