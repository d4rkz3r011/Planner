package creations.nag.planner;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import creations.nag.planner.models.Model;
import creations.nag.planner.month.MonthActivity;

/**
 * This is just a placeholder so the Model and database will only be initialized once
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Model.SINGLETON.initModel(getBaseContext());
        Intent intent = new Intent(getBaseContext(), MonthActivity.class);
        intent.putExtra("startup_date", Model.SINGLETON.getSavedDate());
        startActivity(intent);
    }
}
