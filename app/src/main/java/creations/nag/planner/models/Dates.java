package creations.nag.planner.models;

/**
 * Created by NAG on 10/24/17.
 */

public class Dates {

    private int day;
    private int month;
    private int year;

    public Dates(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    @Override
    public String toString() {
        return "Dates{" +
                "day = " + day +
                ", month = " + month +
                ", year = " + year +
                '}';
    }

    // GETTERS AND SETTERS
    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
