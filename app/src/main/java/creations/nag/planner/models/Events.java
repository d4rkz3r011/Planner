package creations.nag.planner.models;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * Created by NAG on 10/20/17.
 */

public class Events {

    // Data members
    private String event;
    private String hour;
    private String hasAlarm;

    // Constructor
    public Events(String event, String hour, String hasAlarm) {
        this.event = event;
        this.hour = hour;
        this.hasAlarm = hasAlarm;
    }

    @Override
    public String toString() {
        return "Events{" +
                "event = '" + event + '\'' +
                ", hour = '" + hour + '\'' +
                ", hasAlarm = '" + hasAlarm + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Events events = (Events) o;

        if (event != null ? !event.equals(events.event) : events.event != null) return false;
        if (hour != null ? !hour.equals(events.hour) : events.hour != null) return false;
        return hasAlarm != null ? hasAlarm.equals(events.hasAlarm) : events.hasAlarm == null;

    }

    @Override
    public int hashCode() {
        int result = event != null ? event.hashCode() : 0;
        result = 31 * result + (hour != null ? hour.hashCode() : 0);
        result = 31 * result + (hasAlarm != null ? hasAlarm.hashCode() : 0);
        return result;
    }

    // SETTERS AND GETTERS
    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getHasAlarm() {
        return hasAlarm;
    }

    public void setHasAlarm(String hasAlarm) {
        this.hasAlarm = hasAlarm;
    }
}