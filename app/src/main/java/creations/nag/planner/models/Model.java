package creations.nag.planner.models;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import creations.nag.planner.adapters.EventAdapter;
import creations.nag.planner.adapters.TodoAdapter;
import creations.nag.planner.calendar.adapters.CalendarAdapter;
import creations.nag.planner.database.DAO;
import creations.nag.planner.database.Database;
import creations.nag.planner.day.DayActivity;
import creations.nag.planner.month.MonthActivity;

/**
 * This class will hold the information used to supply the app with data.
 *
 * Created by NAG on 10/10/17.
 */

public class Model {

    // SINGLETON method
    public static Model SINGLETON = new Model();

    // Data members
    private DayActivity dayActivity;
    private MonthActivity monthActivity;
    private List<String> listOfHours24;
    private String[] listofHours12;
    private String savedDate;
    private Database db; // Only create the database once
    private HashMap<String, LinkedHashSet<Todos>> todos; // LinkedHashSet preserves order of added Todos
    private HashMap<String, LinkedHashSet<Events>> events;
    private HashMap<String, HashMap<String, LinkedHashSet<Events>>> hoursToevents;
    private HashMap<String, HashSet<Dates>> dates;
    private CalendarAdapter calendarAdapter;
    private int day;
    private int month;
    private int year;
    private TodoAdapter todoAdapter;
    private EventAdapter eventAdapter;

    // Constructor
    private Model() {
        dayActivity = null;
        monthActivity = null;
        listOfHours24 = new ArrayList<>(); //24 since there's 24 hours in a day
        listofHours12 = new String[24];
        savedDate = "";
        todos = new HashMap<>();
        events = new HashMap<>();
        dates = new HashMap<>();
        calendarAdapter = null;
        day = -1;
        month = -1;
        year = -1;
    }

    public void initModel(Context baseContext) {
        initHours12();
        initHours24();
        db = new Database(baseContext);

        String year = new SimpleDateFormat("yyy", Locale.US).format(new Date());
        String month = Integer.toString(Integer.parseInt(new SimpleDateFormat("MM", Locale.US).format(new Date())) - 1); // Subtract 1 since January starts at index 0.
        String date = new SimpleDateFormat("d", Locale.US).format(new Date());
        savedDate = year + month + date; // Formate of yyyymmdd as numbers

        initDates();
    }

    private void initHours24() {
        String minutes = ":00";
        for (int i = 0; i < 24; i++) {
            listOfHours24.add(Integer.toString(i) + minutes);
        }
    }

    private void initHours12() {
        String minutesAM = ":00am";
        String minutesPM = ":00pm";
        for (int i = 1; i < 13; i++) { // Do AM hours first
            listofHours12[i] = Integer.toString(i) + minutesAM;
            if (i == 12) { listofHours12[i] = Integer.toString(i) + minutesPM; } // Need condition for 12pm
        }
        for (int i = 1; i < 12; i++) { // Now do PM hours
            listofHours12[i] = Integer.toString(i) + minutesPM;
        }
    }

    private void initDates() {
        DAO.SINGLETON.getDates(dates);
    }

    public Boolean doesDateExist(String date) {
        return dates.containsKey(date);
    }

    public void updateDates() {
        if (!doEventsTodosExist(savedDate)) {
            DAO.SINGLETON.deleteDates(savedDate); // Only delete notes if there's no events or todos for a day
        }
        DAO.SINGLETON.getDates(dates);
    }

    public void updateTodos() {
        DAO.SINGLETON.getTodos(savedDate, todos);
    }

    public void updateEvents() {
        DAO.SINGLETON.getEvents(savedDate, events);
    }


    /**
     * Checks if the to-do string is already in the map.
     * If it is, it gets replaced by the new to-do title.
     * @param oldTodo
     * @param newTodo
     * @return
     */
    public boolean replaceTodoByTodo(String oldTodo, String newTodo) {
        for (Map.Entry<String, LinkedHashSet<Todos>> map : todos.entrySet()) {
            for (Iterator<Todos> t = map.getValue().iterator(); t.hasNext();) {
                Todos temp = t.next();
                if (temp.getTodo().equals(oldTodo)) {
                    temp.setTodo(newTodo);
                    return true;
                }
            }
        }
        return false;
    }

    public boolean doesTodoExist(String todo) {
        for (Map.Entry<String, LinkedHashSet<Todos>> map : todos.entrySet()) {
            for (Iterator<Todos> t = map.getValue().iterator(); t.hasNext();) {
                Todos temp = t.next();
                if (temp.getTodo().equals(todo)) {
                    return true;
                }
            }
        }
        return false;
    }

    public int getHourForTodo(String todo) {
        for (Map.Entry<String, LinkedHashSet<Todos>> map : todos.entrySet()) {
            for (Iterator<Todos> t = map.getValue().iterator(); t.hasNext();) {
                Todos temp = t.next();
                for (int i = 0; i < listOfHours24.size(); i++) {
                    if (temp.getTodo().equals(todo) && temp.getHour().equals(listOfHours24.get(i))) {
                        return i;
                    }
                }
            }
        }
        return 0;
    }

    public String getCheckmarkForTodo(String todo) {
        for (Map.Entry<String, LinkedHashSet<Todos>> map : todos.entrySet()) {
            for (Iterator<Todos> t = map.getValue().iterator(); t.hasNext();) {
                Todos temp = t.next();
                if (temp.getTodo().equals(todo)) {
                    return temp.getCheckmark();
                }
            }
        }
        return "false";
    }

    public Boolean getAlarmForTodo(String todo) {
        for (Map.Entry<String, LinkedHashSet<Todos>> map : todos.entrySet()) {
            for (Iterator<Todos> t = map.getValue().iterator(); t.hasNext();) {
                Todos temp = t.next();
                for (int i = 0; i < listOfHours24.size(); i++) {
                    if (temp.getTodo().equals(todo) && temp.getHasAlarm().equals("true")) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
            }
        }
        return false;
    }

        /* ~~~~~~~~~~~~~~~~~~~~~~ EVENTS ~~~~~~~~~~~~~~~~~~~~~~ */

    /**
     * Checks the events map if there's an event at a specific hour is already in the map.
     * If it is, then return true to not add to the database.
     * If it's not, then return false to add to the database.
     * @param hour
     * @return
     */
    public boolean doesHourHaveEvent(String hour) {
        for (Map.Entry<String, LinkedHashSet<Events>> map : events.entrySet()) {
            for (Iterator<Events> e = map.getValue().iterator(); e.hasNext();) {
                Events temp = e.next();
                if (temp.getHour().equals(hour)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Checks if the event string is already in the map.
     * If it is, it gets replaced by the new event title.
     * @param eventTodoHour
     * @param title
     * @param isEdit
     * @return
     */
    public String editEventAtHour(String eventTodoHour, String title, Boolean isEdit) {
        for (Map.Entry<String, LinkedHashSet<Events>> map : events.entrySet()) {
            for (Iterator<Events> e = map.getValue().iterator(); e.hasNext();) {
                Events temp = e.next();
                if (temp.getHour().equals(eventTodoHour)) {
                    String newTitle = "";
                    if (!isEdit) {
                        newTitle = temp.getEvent() + "\n" + title;
                        return newTitle;
                    }
                    else {
                        temp.setEvent(title);
                        return title;
                    }
                }
            }
        }
        return title;
    }

    public String getEventAtHour(String eventTodoHour) {
        for (Map.Entry<String, LinkedHashSet<Events>> map : events.entrySet()) {
            for (Iterator<Events> e = map.getValue().iterator(); e.hasNext();) {
                Events temp = e.next();
                if (temp.getHour().equals(eventTodoHour)) {
                    return temp.getEvent(); // Return the event at the hour for updating database
                }
            }
        }
        return ""; // Return nothing if there's no event at the specific hour
    }

    public boolean doEventsTodosExist(String selectedDate) {
        Boolean eventsContains = events.containsKey(selectedDate);
        Boolean todosContains = todos.containsKey(selectedDate);
        return (eventsContains || todosContains);
    }

    public int getHourForEvent(String event) {
        for (Map.Entry<String, LinkedHashSet<Events>> map : events.entrySet()) {
            for (Iterator<Events> e = map.getValue().iterator(); e.hasNext();) {
                Events temp = e.next();
                for (int i = 0; i < listOfHours24.size(); i++) {
                    if (temp.getEvent().equals(event) && temp.getHour().equals(listOfHours24.get(i))) {
                        return i;
                    }
                }
            }
        }
        return 0;
    }

    public Boolean getAlarmForEvent(String todo) {
        for (Map.Entry<String, LinkedHashSet<Events>> map : events.entrySet()) {
            for (Iterator<Events> e = map.getValue().iterator(); e.hasNext();) {
                Events temp = e.next();
                for (int i = 0; i < listOfHours24.size(); i++) {
                    if (temp.getEvent().equals(todo) && temp.getHasAlarm().equals("true")) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
            }
        }
        return false;
    }

    // Getters and Setters
    public DayActivity getDayActivity() {
        return dayActivity;
    }

    public void setDayActivity(DayActivity dayActivity) {
        this.dayActivity = dayActivity;
    }

    public MonthActivity getMonthActivity() {
        return monthActivity;
    }

    public void setMonthActivity(MonthActivity monthActivity) {
        this.monthActivity = monthActivity;
    }

    public List<String> getListOfHours24() { return listOfHours24; };

    public String[] getListOfHours12() { return listofHours12; };

    public String getSavedDate() {
        return savedDate;
    }

    public void setSavedDate(String savedDate) {
        this.savedDate = savedDate;
    }

    public Database getDb() {
        return db;
    }

    public void setDb(Database db) { this.db = db; }

    public HashMap<String, LinkedHashSet<Todos>> getTodos() {
        return todos;
    }

    public void setTodos(HashMap<String, LinkedHashSet<Todos>> todos) {
        this.todos = todos;
    }

    public HashMap<String, LinkedHashSet<Events>> getEvents() {
        return events;
    }

    public void setEvents(HashMap<String, LinkedHashSet<Events>> events) {
        this.events = events;
    }

    public HashMap<String, HashSet<Dates>> getDates() {
        return dates;
    }

    public void setDates(HashMap<String, HashSet<Dates>> dates) {
        this.dates = dates;
    }

    public CalendarAdapter getCalendarAdapter() {
        return calendarAdapter;
    }

    public void setCalendarAdapter(CalendarAdapter calendarAdapter) {
        this.calendarAdapter = calendarAdapter;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public TodoAdapter getTodoAdapter() {
        return todoAdapter;
    }

    public void setTodoAdapter(TodoAdapter todoAdapter) {
        this.todoAdapter = todoAdapter;
    }

    public EventAdapter getEventAdapter() {
        return eventAdapter;
    }

    public void setEventAdapter(EventAdapter eventAdapter) {
        this.eventAdapter = eventAdapter;
    }

}
