package creations.nag.planner.models;

/**
 * Created by NAG on 10/20/17.
 */

public class Todos {

    // Data members
    private String todo;
    private String checkmark;
    private String hour;
    private String hasAlarm;

    // Constructor
    public Todos(String todo, String hour, String checkmark, String hasAlarm) {
        this.todo = todo;
        this.hour = hour;
        this.checkmark = checkmark;
        this.hasAlarm = hasAlarm;
    }

    @Override
    public String toString() {
        return "Todos{" +
                "todo = '" + todo + '\'' +
                ", checkmark = '" + checkmark + '\'' +
                ", hour = '" + hour + '\'' +
                ", hasAlarm = '" + hasAlarm + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Todos todos = (Todos) o;

        if (todo != null ? !todo.equals(todos.todo) : todos.todo != null) return false;
        if (checkmark != null ? !checkmark.equals(todos.checkmark) : todos.checkmark != null)
            return false;
        if (hour != null ? !hour.equals(todos.hour) : todos.hour != null) return false;
        return hasAlarm != null ? hasAlarm.equals(todos.hasAlarm) : todos.hasAlarm == null;

    }

    @Override
    public int hashCode() {
        int result = todo != null ? todo.hashCode() : 0;
        result = 31 * result + (checkmark != null ? checkmark.hashCode() : 0);
        result = 31 * result + (hour != null ? hour.hashCode() : 0);
        result = 31 * result + (hasAlarm != null ? hasAlarm.hashCode() : 0);
        return result;
    }

    // GETTERS AND SETTERS
    public String getTodo() {
        return todo;
    }

    public void setTodo(String todo) {
        this.todo = todo;
    }

    public String getCheckmark() {
        return checkmark;
    }

    public void setCheckmark(String checkmark) {
        this.checkmark = checkmark;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getHasAlarm() {
        return hasAlarm;
    }

    public void setHasAlarm(String hasAlarm) {
        this.hasAlarm = hasAlarm;
    }
}
