package creations.nag.planner.day;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.joanzapata.android.iconify.IconDrawable;
import com.joanzapata.android.iconify.Iconify;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Map;

import creations.nag.planner.adapters.EventAdapter;
import creations.nag.planner.database.DAO;
import creations.nag.planner.models.Events;
import creations.nag.planner.models.Model;
import creations.nag.planner.R;
import creations.nag.planner.SettingsActivity;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DayFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DayFragment extends Fragment {

    // Data members
    private TextView day;
    private Boolean isKeyboardShowing;

    public DayFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment DayFragment.
     */
    public static DayFragment newInstance() {
        DayFragment fragment = new DayFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Nathan: Closes keyboard on fragment start up.
     * Keyboard pops up instantly because of the EditText for the chat
     * @param view view of the activity
     */
    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void showKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(view.getRootView(), 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_day, container, false);
        isKeyboardShowing = false;

        day = (TextView) view.findViewById(R.id.textview_day);

        int month = getActivity().getIntent().getIntExtra("month", 0) - 1; // Subtract 1 since January starts at index 0.
        int date = getActivity().getIntent().getIntExtra("date", 0);
        int year = getActivity().getIntent().getIntExtra("year", 0);
        day.setText(new SimpleDateFormat("EEE MMM d, yyyy", Locale.US).format(new Date(year-1900, month, date))); // Subtract 1900 to compensate for Date() starting with 1900.
        saveSelectedDate(Integer.toString(year), Integer.toString(month), Integer.toString(date)); // The date selected is saved

        RecyclerView listOfEvents = (RecyclerView) view.findViewById(R.id.recyclerview_listOfHours);
        listOfEvents.setLayoutManager(new LinearLayoutManager(getActivity()));
        listOfEvents.setAdapter(new EventAdapter(getActivity()));
        Model.SINGLETON.setEventAdapter((EventAdapter) listOfEvents.getAdapter());


        final FloatingActionButton addEvent = (FloatingActionButton) view.findViewById(R.id.button_addEvent);
        addEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addEvent();
            }
        });

        listOfEvents.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (!isKeyboardShowing) {
                    showKeyboard(v);
                    isKeyboardShowing = true;
                }
                else {
                    hideKeyboard(v);
                    isKeyboardShowing = false;
                }
                return false;
            }
        });

        RelativeLayout frame = (RelativeLayout) view.findViewById(R.id.dayholderlayout);
        frame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isKeyboardShowing) {
                    showKeyboard(v);
                    isKeyboardShowing = true;
                }
                else {
                    hideKeyboard(v);
                    isKeyboardShowing = false;
                }
            }
        });

        loadEvents();
        return view;
    }

    /**
     * Saves the selected date to the Model.
     * So when things need to be saved to the database, it can grab data easily.
     *
     * @param year Year of selected date
     * @param month Month of selected date
     * @param date Date of selected date
     */
    private void saveSelectedDate(String year, String month, String date) {
        String savedDate = year + month + date; // Format of yyyymmdd as numbers
        Model.SINGLETON.setSavedDate(savedDate);
        Model.SINGLETON.setDay(Integer.parseInt(date));
        Model.SINGLETON.setMonth(Integer.parseInt(month));
        Model.SINGLETON.setYear(Integer.parseInt(year));
    }

    public void addEvent() {
        Intent intent = new Intent(getActivity(), AddEventActivity.class);
        intent.putExtra("Add Event", "Event");
        startActivity(intent);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.daymonth_options, menu);

        MenuItem settings = menu.findItem(R.id.settingsButton);
        settings.setIcon(new IconDrawable(getActivity(), Iconify.IconValue.fa_gear).actionBarSize().colorRes(R.color.menuItemColor));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.settingsButton:
                startActivity(new Intent(getActivity(), SettingsActivity.class));
                break;
            default:
                break;
        }
        return true;
    }

    private void loadEvents() {
        DAO.SINGLETON.getEvents(Model.SINGLETON.getSavedDate(), Model.SINGLETON.getEvents());
    }
}
