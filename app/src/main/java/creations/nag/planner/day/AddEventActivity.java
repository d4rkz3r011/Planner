package creations.nag.planner.day;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import java.util.Calendar;
import java.util.TimeZone;

import creations.nag.planner.R;
import creations.nag.planner.adapters.TimeSpinnerAdapter;
import creations.nag.planner.database.DAO;
import creations.nag.planner.models.Model;

public class AddEventActivity extends AppCompatActivity {

    private EditText eventTodoTitle;
    private String isEventTodo;
    private String eventTodoHour;
    private String savedDate;
    private Button delete;
    private TextView activityTitle;
    private TextView whatTime;
    private Switch alarm;
    private Spinner time;
    private String hasAlarm;
    private String oldTitle;
    private String isChecked;
    private int hour_position;
    private Boolean isKeyboardShowing;
    private Boolean isEdit;
    private Intent intent;
    private Model model;
    private DAO dao;

    /**
     * Nathan: Closes keyboard on fragment start up.
     * Keyboard pops up instantly because of the EditText for the chat
     * @param view view of the activity
     */
    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager) this.getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void showKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager) this.getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(view.getRootView(), 0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_event);

        intent = getIntent();
        model = Model.SINGLETON;
        dao = DAO.SINGLETON;
        savedDate = model.getSavedDate();
        isKeyboardShowing = false;
        isEdit = false;
        hour_position = 0;
        isChecked = "false";
        hasAlarm = "false";


        eventTodoTitle = (EditText) findViewById(R.id.edittext_eventTodoTitle);
        eventTodoTitle.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v); // Hides keyboard when leaving the notes page
                }
            }
        });


        alarm = (Switch) findViewById(R.id.switch_alarm);
        alarm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    hasAlarm = "true";
                }
                else {
                    hasAlarm = "false";
                }
            }
        });


        Button save = (Button) findViewById(R.id.button_saveEventTodo);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSavePressed();
                finish(); // Closes the view after hitting save
            }
        });

        Button cancel = (Button) findViewById(R.id.button_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish(); // Closes the view and can't press the back button to see it again.
            }
        });

        delete = (Button) findViewById(R.id.button_delete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDeletePressed();
                finish();
            }
        });

        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.relativelayout_addEventTodo);
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isKeyboardShowing) {
                    showKeyboard(v);
                    isKeyboardShowing = true;
                }
                else {
                    hideKeyboard(v);
                    isKeyboardShowing = false;
                }
            }
        });

        activityTitle = (TextView) findViewById(R.id.textview_addEventTodoTitle);
        whatTime = (TextView) findViewById(R.id.textview_whatTime);

        setDoIntent();

        time = (Spinner) findViewById(R.id.spinner_time);
        time.setOnItemSelectedListener(new TimeSpinnerAdapter(time));
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
                model.getListOfHours24());
        time.setAdapter(adapter);
        if (isEventTodo.equals("Event")) {
            time.setVisibility(View.VISIBLE);
            whatTime.setVisibility(View.VISIBLE);
        }
        time.setSelection(hour_position, true);
    }

    private int getCurrentTimePos() {
        TimeZone timeZone = Calendar.getInstance().getTimeZone();
        int startHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        int startMinute = Calendar.getInstance().get(Calendar.MINUTE);
        int getTimeOffset = timeZone.getRawOffset();

//        long startTimeMillis = (startHour * 1000 * 60 * 60) + (startMinute * 1000 * 60);
//        startTimeMillis  -= timegetTimeOffset(startTimeMillis);

        int index = Model.SINGLETON.getListOfHours24().indexOf(Integer.toString(startHour) + ":00");
        return index;
    }

    private void setDoIntent() {
        if (intent.hasExtra("Add Event")) {
            activityTitle.setText("Add Event");
            isEventTodo = "Event";
            whatTime.setText("Event time?");
        }
        else if (intent.hasExtra("Edit Event")) {
            activityTitle.setText("Edit Event");
            isEventTodo = "Event";
            oldTitle = intent.getExtras().getString("Edit Event");
            hour_position = intent.getExtras().getInt("hour");
            alarm.setChecked(intent.getExtras().getBoolean("alarm"));
            eventTodoTitle.setText(oldTitle);
            delete.setVisibility(View.VISIBLE);
            isEdit = true;
            whatTime.setText("Event time?");
        }
        else if (intent.hasExtra("Add Todo")) {
            activityTitle.setText("Add Todo");
            isEventTodo = "Todo";
            whatTime.setText("Todo time?");
        }
        else if (intent.hasExtra("Edit Todo")) {
            activityTitle.setText("Edit Todo");
            isEventTodo = "Todo";
            oldTitle = intent.getExtras().getString("Edit Todo");
            hour_position = intent.getExtras().getInt("hour");
            isChecked = intent.getExtras().getString("checkmark");
            alarm.setChecked(intent.getExtras().getBoolean("alarm"));
            eventTodoTitle.setText(oldTitle);
            delete.setVisibility(View.VISIBLE);
            isEdit = true;
            whatTime.setText("Todo time?");
        }
    }

    private void onSavePressed() {
        eventTodoHour = time.getSelectedItem().toString(); // Get the selected time
        String title = eventTodoTitle.getText().toString();
        if (alarm == null) { hasAlarm = "false"; } // Set alarm to false if needed

        if (isEventTodo.equals("Event")) {
            if (model.doesHourHaveEvent(eventTodoHour)) { // Checks if there's an event at hour chosen
                oldTitle = model.getEventAtHour(eventTodoHour); // Grab old title
                title = model.editEventAtHour(eventTodoHour, eventTodoTitle.getText().toString(), isEdit); // Now change it
            }
            dao.putEvents(savedDate, title, eventTodoHour, hasAlarm, oldTitle);
            model.getEventAdapter().updateEventsInView();
        }
        else if (isEventTodo.equals("Todo")) {
            // A to-do can't be marked off as completed when it's newly created
            dao.putTodos(savedDate, title, eventTodoHour, isChecked, hasAlarm, oldTitle);
            model.getTodoAdapter().updateTodosInView();
        }
        dao.putDates(savedDate);
        model.updateDates();
    }

    private void onDeletePressed() {
        if (isEventTodo.equals("Event")) {
            dao.deleteEvent(eventTodoTitle.getText().toString());
            model.getEventAdapter().updateEventsInView();
        }
        else if (isEventTodo.equals("Todo")) {
            dao.deleteTodo(eventTodoTitle.getText().toString());
            model.getTodoAdapter().updateTodosInView();
        }
        model.updateDates();
    }
}
