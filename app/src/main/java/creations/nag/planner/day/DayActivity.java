package creations.nag.planner.day;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;

import creations.nag.planner.models.Model;
import creations.nag.planner.R;
import creations.nag.planner.SettingsActivity;
import creations.nag.planner.adapters.DayPageSliderAdapter;

public class DayActivity extends AppCompatActivity {

    private DayFragment dayFragment;
    private DayNoteFragment dayNoteFragment;
    private DayTodoFragment dayTodoFragment;

    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private ViewPager viewPager;

    /**
     * The page adapter, which provides the pages to the view pager widget.
     */
    private DayPageSliderAdapter dayPageSliderAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        setContentView(R.layout.activity_day);

        getActionBar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.view_pager_day);
        dayPageSliderAdapter = new DayPageSliderAdapter(this.getSupportFragmentManager());
        viewPager.setAdapter(dayPageSliderAdapter);
        viewPager.setCurrentItem(1);

        // Create the fragments
        if (dayFragment == null) {
            dayFragment = DayFragment.newInstance();

        }
        if (dayNoteFragment == null) {
            dayNoteFragment = DayNoteFragment.newInstance();

        }
        if (dayTodoFragment == null) {
            dayTodoFragment = DayTodoFragment.newInstance();

        }

        if (savedInstanceState != null) { //Sends the date selected
            dayFragment.setArguments(getIntent().getExtras());
        }

        Model.SINGLETON.setDayActivity(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.daymonth_options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.settingsButton:
                startActivity(new Intent(getBaseContext(), SettingsActivity.class));
                break;
            case android.R.id.home:
                finish();
                return true;
            default:
                break;
        }
        return true;
    }



    // Getters and setters
    public DayFragment getDayFragment() {
        return dayFragment;
    }

    public void setDayFragment(DayFragment dayFragment) {
        this.dayFragment = dayFragment;
    }

    public DayNoteFragment getDayNoteFragment() {
        return dayNoteFragment;
    }

    public void setDayNoteFragment(DayNoteFragment dayNoteFragment) {
        this.dayNoteFragment = dayNoteFragment;
    }

    public DayTodoFragment getDayTodoFragment() {
        return dayTodoFragment;
    }

    public void setDayTodoFragment(DayTodoFragment dayTodoFragment) {
        this.dayTodoFragment = dayTodoFragment;
    }

    public void update() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Model.SINGLETON.getTodoAdapter().notifyDataSetChanged();
            }
        });
    }
}
