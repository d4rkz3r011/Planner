package creations.nag.planner.day;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.sql.SQLException;

import creations.nag.planner.R;
import creations.nag.planner.database.DAO;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DayNoteFragment} interface
 * to handle interaction events.
 * Use the {@link DayNoteFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DayNoteFragment extends Fragment {

    private EditText notes;
    private Boolean isKeyboardShowing;
    Boolean notesExist;

    public DayNoteFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment DayNoteFragment.
     */
    public static DayNoteFragment newInstance() {
        DayNoteFragment fragment = new DayNoteFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Nathan: Closes keyboard on fragment start up.
     * Keyboard pops up instantly because of the EditText for the chat
     * @param view view of the activity
     */
    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void showKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(view.getRootView(), 0);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_day_note, container, false);


        isKeyboardShowing = false;
        notes = (EditText) view.findViewById(R.id.edittext_dayNotes);
        notes.setClickable(true);
        notes.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v); // Hides keyboard when leaving the notes page
                    try {
                        if (!notes.getText().toString().equals("")) {
                            saveNotes(notes.getText().toString());
                        }
                    } catch (Exception e) { Log.d("@saveNotes ERROR", e.getMessage()); }
                }
            }
        });
        notes.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                showDialog();
                return false;
            }
        });

        // This allows the keyboard to be hidden when tapping outside of the edittext
        RelativeLayout frame = (RelativeLayout) view.findViewById(R.id.daynoteholderlayout);
        frame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isKeyboardShowing) {
                    showKeyboard(v);
                    isKeyboardShowing = true;
                }
                else {
                    hideKeyboard(v);
                    isKeyboardShowing = false;
                    try {
                        if (!notes.getText().toString().equals("")) {
                            saveNotes(notes.getText().toString());
                        }
                    } catch (Exception e) { Log.d("@saveNotes ERROR", e.getMessage()); }
                }
            }
        });
        frame.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                showDialog();
                return false;
            }
        });

        loadNotes();
        return view;
    }

    private void showDialog() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        DAO.SINGLETON.deleteNotes();
                        notes.setText("");
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Do you want to delete all notes for today?");
        builder.setPositiveButton("Yes", dialogClickListener);
        builder.setNegativeButton("No", dialogClickListener).show();
    }

    private void saveNotes(String notes) throws SQLException {
        DAO.SINGLETON.putNotes(notes, notesExist);
    }

    private void loadNotes() {
        notesExist = false;
        try {
            String check = DAO.SINGLETON.getNotes();
            if (!check.equals("")) {
                this.notes.setText(check);
                notesExist = true;
            }

        } catch (Exception e) { Log.d("@loadNotes-ERROR", e.getMessage()); }
    }
}
