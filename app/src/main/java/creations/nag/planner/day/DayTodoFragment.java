package creations.nag.planner.day;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;

import creations.nag.planner.adapters.TodoAdapter;
import creations.nag.planner.R;
import creations.nag.planner.database.DAO;
import creations.nag.planner.models.Model;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link DayTodoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DayTodoFragment extends Fragment {

    private Boolean isKeyboardShowing;

    public DayTodoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment DayTodoFragment.
     */
    public static DayTodoFragment newInstance() {
        DayTodoFragment fragment = new DayTodoFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void showKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(view.getRootView(), 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_day_todo, container, false);

        isKeyboardShowing = false;

        FloatingActionButton todo = (FloatingActionButton) view.findViewById(R.id.button_addTodo);
        todo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    addTodo();
                } catch (Exception e) {
                    Log.d("Adding new todo", e.getMessage());
                }
            }
        });

        RecyclerView listOfTodo = (RecyclerView) view.findViewById(R.id.recyclerview_listOfTodos);
        listOfTodo.setLayoutManager(new LinearLayoutManager(getActivity()));
        listOfTodo.setAdapter(new TodoAdapter(getActivity()));
        Model.SINGLETON.setTodoAdapter((TodoAdapter) listOfTodo.getAdapter());

        listOfTodo.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (!isKeyboardShowing) {
                    showKeyboard(v);
                    isKeyboardShowing = true;
                }
                else {
                    hideKeyboard(v);
                    isKeyboardShowing = false;
                }
                return false;
            }
        });

        // This allows the keyboard to be hidden when tapping outside of the edittext
        RelativeLayout frame = (RelativeLayout) view.findViewById(R.id.todoholderlayout);
        frame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isKeyboardShowing) {
                    showKeyboard(v);
                    isKeyboardShowing = true;
                }
                else {
                    hideKeyboard(v);
                    isKeyboardShowing = false;
                }
            }
        });

        loadTodos();
        return view;
    }

    public void addTodo() throws InterruptedException {
        Intent intent = new Intent(getActivity(), AddEventActivity.class);
        intent.putExtra("Add Todo", "Todo");
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();

    }


    private void loadTodos() {
        DAO.SINGLETON.getTodos(Model.SINGLETON.getSavedDate(), Model.SINGLETON.getTodos());
    }
}
